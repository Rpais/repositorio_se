// Aula01bibliotecaStats.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "biblioteca.h"
#include <stdio.h>

#define TAMV 10

int main()
{
	int v[TAMV] = { 10,2,5,1,6,3,4,8,9,7 };
	bSort(v,TAMV);
	printf("\n\nMax: %d", maxVectOrd(v, TAMV));
	printf("\n\nMin: %d", minVectOrd(v, TAMV));
	printf("\n\nAverage: %.2f", averageV(v, TAMV));
	printf("\n\nPercentil 25: %d", percentil_x_VectOrd(v, TAMV, 25));
	printf("\n\nPercentil 50: %d", percentil_x_VectOrd(v, TAMV, 50));
	printf("\n\nPercentil 75: %d", percentil_x_VectOrd(v, TAMV, 75));
	getchar();
	
	return 0;
}