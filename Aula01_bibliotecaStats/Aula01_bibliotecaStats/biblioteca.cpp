#include <stdio.h>
#include "stdafx.h"

//////////////////////////////////////////////////////
//m�todo para oredena��o de vetor por ordem crescente
//////////////////////////////////////////////////////
int bSort(int v[], int tamv)
{
	printf("Vetor introduzido:\n");
	for (int i = 0; i < tamv; i++)
		printf("%d ", v[i]);

	//ordenacao:
	//1� - garantir que quando o maior estiver colocado no fim j� n�o � mexido...
	for (int fim = tamv - 1; fim > 0; --fim)
		//2� - analisar do primeiro at� ao "fim" (por itera��o) se o elemento atual � maior que o seguinte
		for (int i = 0; i < fim; ++i) 
			if (v[i] > v[i + 1]) 
			{
				//maior passa para a sua direita
				int aux = v[i];
				v[i] = v[i + 1];
				v[i + 1] = aux;
			}

	printf("\n\nVetor ordenado:\n");
	for (int i = 0; i < tamv; i++)
		printf("%d ", v[i]);

	return *v;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//m�todo para determina��o do maior elemento do vetor ( o ultimo se estiver ordenado por ordem crescente)
////////////////////////////////////////////////////////////////////////////////////////////////////////////
int maxVectOrd(int vOrd[], int tamv)
{
	return vOrd[tamv-1];
	// NOTA - pode-se inclusivamente avaliar de que maneira o vetor vem ordenado... 
	//      - se for por odem crescente ser� v[tamv] 
	//      - senao ser� v[0]
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//m�todo para determina��o do menor elemento do vetor ( o primeiro se estiver ordenado por ordem crescente)
////////////////////////////////////////////////////////////////////////////////////////////////////////////
int minVectOrd(int vOrd[], int tamv)
{
	return vOrd[0];
	// NOTA - pode-se inclusivamente avaliar de que maneira o vetor vem ordenado... 
	//      - se for por odem crescente ser� v[tamv] 
	//      - senao ser� v[0]
}


///////////////////////////////////////////////////////////
//m�todo para determina��o da m�dia dos elementos do vetor
///////////////////////////////////////////////////////////
float averageV(int vOrd[], int tamv)
{
	float sum = 0;
	for (int i = 0; i < tamv; i++)
		sum += vOrd[i];
	return sum / tamv;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//m�todo para determina��o do percentil desejado - assume-se que a ordem � crescente!
//pode-se confirmar aqui - http://www.emathhelp.net/calculators/probability-statistics/percentile-calculator/?i=1%2C2%2C3%2C4%2C5%2C6%2C7%2C8%2C9%2C10&p=75&steps=on
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int percentil_x_VectOrd(int vOrd[], int tamv, int perc)
{
	int posPercentil = tamv*perc / 100;

	printf("\n\nElementos no percentil %d:\n",perc);
	for (int i = 0; i < posPercentil; i++)
		printf("%d ", vOrd[i]);

	return vOrd[posPercentil];
}